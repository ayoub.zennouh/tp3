package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {

    WeatherResponse weatherInfo;
    City cityInfo;

    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setLastUpdate(System.currentTimeMillis()/1000);
    }
}
