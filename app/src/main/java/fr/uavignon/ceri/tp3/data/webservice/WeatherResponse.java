package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResponse {

    public final Main main = null;
    public final Wind wind = null;
    public final List<Weather> weather = null;
    public final Clouds clouds = null;

    public static class Weather {
        public int id = 0;
        public String main = "";
        public String description = "";
        public String icon = "";
    }
    public static class Main{
        public float temp = 0;
        public float feels_like = 0;
        public float temp_min = 0;
        public float temp_max = 0;
        public int pressure = 0;
        public int humidity = 0;
    }

    public static class Wind {
        public float speed = 0;
        public int deg = 0;
        public float gust = 0;
    }

    public static class Clouds {
        public int all = 0;
    }

}
